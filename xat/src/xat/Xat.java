

/*
 * XatServer ens permet conectar amb l'altre part del xat gracies als sockets i enviar missatges entre client i servidor
 * Autor: Ferran López
 * Data: 6/4/2019
 */

//package
package xat;


//imports
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;


public class Xat {

    private static final int PORT = 5000;
    
    public static void main(String[] args) {
        
        //demanem la ip del servidor al usuari
        System.out.println("IP del servidor: ");
        String ip = new Scanner(System.in).nextLine();
        
        
       try{
           //conectem al servidor amb la ip i el port
           Socket socket = new Socket(ip, PORT);
           
           String msg ="";
           
           while(!msg.equals("FI")){
               System.out.println("Envia un missatge: ");
               msg = new Scanner(System.in).nextLine();
               enviarString(socket, msg);
               
               if(!msg.equals("FI")){
                   msg = rebreString(socket);
                   if(!msg.equals("FI")){
                       System.out.println("Servidor diu: " + msg);
                   }
               }
           }
           
           System.out.println("Final del programa");
           
       }catch(Exception e){
           System.out.println(e.toString());
                   
       }
    }
    
    /*
     * Funcio enviarString, rep un objecte del tipus Socket com a parametre i un String que es la cadena que volem enviar
     * Llença una excepcio
     * Ens permet mitjançant el OutputStream del socket enviar un objecte de tipus String
    */ 
    private static void enviarString(Socket socket, String msg) throws Exception
    {
        try
        {
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(msg);
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    /*
     * Funcio rebreString, rep un objecte del tipus Socket com a parametre i llença una excepcio
     * Ens permet mitjançant el InputStream del socket rebre un objecte de tipus String
    */ 
    private static String rebreString(Socket socket) throws Exception
    {
        String msg;
        
        try
        {
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            msg = (String) ois.readObject();
        }
        catch(Exception e)
        {
            throw e;
        }
        return msg;
    }
    
}
