/*
 * Clase XinosServer.java, ens permet hostejar el joc dels "xinos"
 * autor: Ferran López Puig
 * Data de creació: 26/5/2019
 */

//Package
package xinosserver;


//Imports
import StreamUtils.StreamUtils;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import xinos.pojo.Jugador;


public class XinosServer {

    private static final int PORT = 5000;
    private static ArrayList<Jugador> jugadors = new ArrayList<>();
    private static ArrayList<String> guanyadors = new ArrayList<>();
    private static ArrayList<Socket> sockets = new ArrayList<>();
    private static int totalMonedes;

    public static void main(String[] args) {

        try {
            //Creem el serverSocket i el Socket
            ServerSocket serverSocket = new ServerSocket(PORT);
            Socket socket;

            int numJugadors = 0;

            do {
                //Demanem el numero de jugadors 
                System.out.println("Numero de jugadors: ");
                numJugadors = new Scanner(System.in).nextInt();

            } while (numJugadors < 2);

            for (int i = 0; i < numJugadors; i++) {

                //Acceptem la conexio
                socket = serverSocket.accept();

                //Afegim el socket al arrayList de sockets
                sockets.add(socket);

                //Guardem el jugador al arrayList de jugadors
                jugadors.add(rebreJugador(socket));

            }

            //Calculem el total de monedes
            totalMonedes = calcularTotalMonedes();

            //Mirem si algun jugador ha acertat
            for (Jugador j : jugadors) {
                if (j.getTotalMonedes() == totalMonedes) {
                    guanyadors.add(j.getNom());
                }
            }

            //Enviem els resultats als clients
            for (Socket s : sockets) {

                try {
                    StreamUtils.enviarArrayListStrings(s, guanyadors);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                //tanquem el socket
                s.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /*
     * Funció rebreJugador
     * Rep un socket
     * rep un Jugador per el socket
     * retorn el jugador rebut
     */
    public static Jugador rebreJugador(Socket socket) {
        ObjectInputStream ois = null;

        Jugador jugador = null;
        try {

            ois = new ObjectInputStream(socket.getInputStream());
            jugador = (Jugador) ois.readObject();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return jugador;
    }

    /*
     * Funció calcularTotalMonedes
     * no rep res
     * calcula el total de monedes dels jugadors
     * retorna el total de monedes;
     */
    public static int calcularTotalMonedes() {
        int numMonedes = 0;

        for (Jugador j : jugadors) {
            numMonedes += j.getNumMonedes();
        }

        return numMonedes;
    }

}
