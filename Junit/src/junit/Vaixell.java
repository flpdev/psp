/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junit;


public class Vaixell {
    
   
    private boolean aLaMar; 
    private String capita; 
    private  int totalPersones = 0;

    public Vaixell(boolean aLaMar, String capita) {
        
        this.aLaMar = aLaMar;
        this.capita = capita;
        
    }
    
   
    
    public int calcularTotalPersones(int passatgers, int personal){
        this.totalPersones = passatgers + personal;
        return this.totalPersones;
    }
    
    public boolean setALaMar(){
        if(this.totalPersones > 10){
            this.aLaMar =  true;
        }else{
            this.aLaMar = false;
        }
        
        return this.aLaMar;
    }
    
}
