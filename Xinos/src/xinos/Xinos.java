/*
 * Clase Xinos.java, ens permet conectar-nos al servidor del joc per jugar als "xinos"
 * autor: Ferran López Puig
 * Data de creació: 26/5/2019
 */

//Package
package xinos;

//Imports
import StreamUtils.StreamUtils;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import xinos.pojo.Jugador;


public class Xinos {

    private static final int PORT = 5000;
    
    public static void main(String[] args) {
        
        
        //Demanem la ip
        System.out.println("IP: ");
        String ip = new Scanner(System.in).nextLine();
        
        
        //Si no ha introduit la ip, agafarem "localhost"
        if("".equals(ip)){
            ip = "localhost";
        }
        
        try{
            //Creem el socket amb la ip i el PORT
            Socket socket = new Socket(ip, PORT); 
            String nom;
            int numMonedes = 0;
            int totalMonedes = 0;
           
            
            //Demanem els valors al jugador
            do{
                
                System.out.println("Nom: ");
                nom = new Scanner(System.in).nextLine();
            }while(nom.equals(""));
           
            do{
                System.out.println("Numero de monedes: ");
                numMonedes = new Scanner(System.in).nextInt();
            }while(numMonedes < 0 || numMonedes > 3);
            
            do{
                System.out.println("Total de monedes: ");
                totalMonedes = new Scanner(System.in).nextInt();
            }while(totalMonedes <0);
            
            
            //Creem el jugador amb els valors introduits
            Jugador jugador = new Jugador(nom, numMonedes, totalMonedes);
           
            //Enviem jugador al servidor
            enviarJugador(socket, jugador);
           
            //Rebem els resultats del servidor i els mostrem per pantalla
            ArrayList<String> guanyadors = StreamUtils.rebreArrayListStrings(socket);
            
            System.out.println("Guanyadors: ");
            
            if(guanyadors.size() > 0){
                for(String s: guanyadors){
                    System.out.println(s + "");
                }
            }else{
                System.out.println("No ha guanyat ningu");
            }
           
 
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
    }
    
    
    
    
    /*
     * Funció enviarJugador
     * Rep un socket i un Jugador
     * envia el jugador per el socket
     * no retorna res
     */
    public static void enviarJugador(Socket socket, Jugador jugador){
        ObjectOutputStream oos = null;
        try {
            
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(jugador);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
    
}
