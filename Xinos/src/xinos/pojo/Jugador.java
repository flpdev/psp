/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xinos.pojo;

import java.io.Serializable;

/**
 *
 * @author Ferran
 */
public class Jugador implements Serializable {
    private String nom;
    private int numMonedes;
    private int totalMonedes;
    
    public Jugador(String nom, int numMonedes, int totalMonedes){
        this.nom = nom;
        this.numMonedes = numMonedes;
        this.totalMonedes = totalMonedes;
    }
    
    public int getNumMonedes(){
        return this.numMonedes;
    }
    
    public int getTotalMonedes(){
        return this.totalMonedes;
    }
    
    public String getNom(){
        return this.nom;    }
}