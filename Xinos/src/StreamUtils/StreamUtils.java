/*
 * StreamUtils.java, recopilació de funcions que ens permet enviar i rebre diferents objectes i tipus primitius per sockets
 * autor: Ferran López
 * data de creació: 26/5/2019
 */

//Package
package StreamUtils;


//Imports
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;


public class StreamUtils {
    
    
    /*
     * Funcio enviarInt
     * rep un socket i un int per parametre
     * envia a traves del socket el int
     * no retorna res
    */
    public static void enviarInt(Socket socket, int num) throws Exception{
        try{
            
            OutputStream is = socket.getOutputStream();
            DataOutputStream dis = new DataOutputStream(is);
            dis.write(num);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
    /*
     * Funcio rebreNumero
     * rep un socket per parametre
     * rep un int del socket
     * retorna un int
    */
    public static int rebreNumero(Socket socket) throws Exception{
        int num=-1;
        try{
            InputStream is = socket.getInputStream();
            num = is.read();
        }catch(Exception e)
        {
            throw e;
        }
        
        return num;
    }
    
    
    /*
     * Funcio enviarByte
     * rep un socket per parametre i un int
     * envia a traves del socket el int rebut com a parametre, que despres sera tractat com un byte
     * no retorna res
    */
    public static void enviarByte(Socket socket, int caracter) throws Exception{
        
        try{
            OutputStream os = socket.getOutputStream();
            os.write(caracter);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
    /*
     * Funcio rebreByte
     * rep un socket per parametre
     * rep un byte del socket
     * retorna un int
   */
    public static int rebreByte(Socket socket) throws Exception{
        int b = 0;
        try{
            InputStream is = socket.getInputStream();
            b = is.read();
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return b;
    }
    
    
    /*
     * Funcio rebreString.
     * rep un Socket com a parametre
     * retorna un msg
     * llença una excepcio
     * rep un string per el socket
    */
    public static String rebreString(Socket socket) throws Exception{
        String msg;
        
        try
        {
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            msg = (String) ois.readObject();
        }
        catch(Exception e)
        {
            throw e;
        }
        return msg;
    }
    
    /*
     * Funcio enviarString.
     * rep un Socket com a parametre i un string, que es la cadena a enviar
     * no retorna res
     * llença una excepcio
     * Envia un string per el Stream del socket
    */
    public static void enviarString(Socket socket, String msg) throws Exception
    {
        try
        {
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(msg);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    
    /*
     * Funcio enviarBoolean.
     * rep un Socket com a parametre i un boolea, que es el boolea a enviar
     * no retorna res
     * llença una excepcio
     * Envia un boolea per el Stream del socket
    */
    public static void enviarBoolean(Socket socket, boolean exiteix) throws Exception{

        try{
            OutputStream is = socket.getOutputStream();
            DataOutputStream dos = new DataOutputStream(is);
            dos.writeBoolean(exiteix);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
	
	
    /*
     * Funcio rebreBoolean.
     * rep un Socket com a parametre
     * retorna un boolea
     * llença una excepcio
     * rep un boolea per el socket
    */
    public static boolean rebreBoolean(Socket socket) throws Exception{
        boolean existeix = false;
        
        try{
            InputStream is = socket.getInputStream();
            DataInputStream dis = new DataInputStream(is);
             existeix = dis.readBoolean();

        }catch(Exception e){
                e.printStackTrace();
        }

        return existeix;
    }
    
    /*
     * Funcio enviarFitxer.
     * rep un socket per parametre, un nom de fitxer i un path
     * no retorna res
     * envia un fitxer byte per byte per el socket
    */
    public static void enviarFitxer(Socket socket, String filename, String path){
        
        try{
            FileInputStream fis = new FileInputStream(path + filename);
            BufferedInputStream bis = new BufferedInputStream(fis);

            int caracter = bis.read();

            while (caracter != -1) {

                StreamUtils.enviarByte(socket, caracter);
                caracter = bis.read();
            }

            bis.close();
            fis.close();

        }catch(Exception e){
            e.printStackTrace();
        }
           
    }
    
    
    /*
     * Funcio enviarFitxer.
     * rep un socket per parametre i un nom de fitxer
     * no retorna res
     * envia un fitxer byte per byte per el socket
    */
    public static void enviarFitxer(Socket socket, String filename){
        
        try{
            FileInputStream fis = new FileInputStream(filename);
            BufferedInputStream bis = new BufferedInputStream(fis);

            int caracter = bis.read();

            while (caracter != -1) {

                enviarByte(socket, caracter);
                caracter = bis.read();
            }

            bis.close();
            fis.close();

        }catch(Exception e){
            e.printStackTrace();
        }
           
    }
    
    /*
     * Funcio rebreFitxer.
     * rep un socket per parametre, un nom de fitxer i un path
     * no retorna res
     * rep un fitxer byte per byte desde el socket
    */
    public static void rebreFitxer(Socket socket, String filename, String path) {

        try {
            
            FileOutputStream fos = new FileOutputStream(path + filename);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int caracter = rebreByte(socket);

            while (caracter != -1) {

                bos.write(caracter);
                caracter = StreamUtils.rebreByte(socket);
            }

            
            bos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    /*
     * Funcio rebreFitxer.
     * rep un socket per parametre i un nom de fitxer
     * no retorna res
     * rep un fitxer byte per byte desde el socket
    */
    public static void rebreFitxer(Socket socket, String filename) {

        try {
            
            FileOutputStream fos = new FileOutputStream(filename);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int caracter = rebreByte(socket);

            while (caracter != -1) {

                bos.write(caracter);
                caracter = StreamUtils.rebreByte(socket);
            }

            
            bos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    /*
     * Funcio enviarArrayListStrings.
     * rep un socket per parametre i un ArrayList de Strings
     * no retorna res
     * llença IOException
     * envia el arrayList linies per el outputStream del Socket
    */
    public static void enviarArrayListStrings(Socket socket, ArrayList<String> array) throws IOException{
        
        try{
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(array);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    /*
     * Funcio rebreArrayList.
     * rep un socket per parametre
     * retorna un ArrayList de Strings amb les linies enviades per el servidor
     * llença IOException
    */
    public static ArrayList<String> rebreArrayListStrings(Socket socket) throws IOException{
        ArrayList<String> array = new ArrayList<>();
        try{
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            array = (ArrayList<String>)ois.readObject();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return array;
    }
}