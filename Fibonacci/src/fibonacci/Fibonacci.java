/*
 * Classe Fibonacci. 
 * Ens permet mitjançant threads, calcular els 40 primers numeros de la serie de 
 * Fibonacci
 * autor: Ferran Lopez, DAM2T
 * data de creació: 4/03/2019
*/

//Package
package fibonacci;


//Imports
import java.util.ArrayList;


public class Fibonacci {

 
    
    private static ArrayList<Integer> fibonacci = new ArrayList<>();
    
    public static void main(String[] args) {
        
        ArrayList<Calculador> calculadors = new ArrayList<>();
        
        inicialitzarFibo(fibonacci);
        Calculador c = new Calculador(fibonacci, "c");
        Calculador c2 = new Calculador(fibonacci, "c2");
        Calculador c3 = new Calculador(fibonacci, "c3");
        Calculador c4 = new Calculador(fibonacci, "c4");
        Calculador c5 = new Calculador(fibonacci, "c5");
        
        calculadors.add(c);
        calculadors.add(c2);
        calculadors.add(c3);
        calculadors.add(c4);
        calculadors.add(c5);
        
        for(Calculador calc: calculadors){
            calc.start();
        }
        
          
    }
    
    
    /*
     * Funcio estatica inicialitzarFibo, no rep ni retorna res
     * Afegeix els dos primers numeros de la serie de Fibonacci, que son 0 i 1
     */
    public static void inicialitzarFibo(ArrayList<Integer> fibo){
        fibo.add(0);
        fibo.add(1);
        
        for(Integer i: fibonacci){
            System.out.println(i);
        }
    }
}
