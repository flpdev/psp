/*
 * Proces Caldulador.java
 * ens permet calcular els 40 primers numeros de la serie de Fibbonacci, i afegirlos a un recurs compartit, en aquest cas un ArrayList.
 * autor: Ferran Lopez, DAM2T
 * data de creació: 4/03/2019
 */

//Package
package fibonacci;

//Imports
import java.util.ArrayList;


public class Calculador extends Thread{
    private static ArrayList<Integer> fibonacci; 
    private static int NUM_REPETICIONS = 8;
    
    public Calculador(ArrayList<Integer> fibo, String nom){
        super(nom);
        this.fibonacci = fibo;
    }
    
    @Override
    public void run(){
        
        for(int i=0; i<NUM_REPETICIONS; i++){
            afegirNumero();
        }
        
    }
    
    
    /*
     * Funcio afegirNumero, no rep ni retorna res
     * Protegeix el arrayList, perque cap process l'utiltizi mentres s'utilitza aqui,
     * i calcula els seguent numero de fibonnaci, l'afegeix al arrayList i el mostra per pantalla
     * tot calculant la proporcio amb el numero anterior
     */
    public void afegirNumero(){
        try{
            synchronized(fibonacci){
                
                int n1 = fibonacci.get(fibonacci.size()-1);
                int n2 = fibonacci.get(fibonacci.size()-2);

                int n = n1 + n2;
                
                //System.out.println(n2 + " " + n1 + " "+ n);
                
                if(n1 != 0 && n1 !=1){
                   
                    System.out.println(this.getName() + ":   " + n + "(" + (float) n / n1 + ")"); 
                }
                fibonacci.add(n);
                
            }
            sleep(200);
        }catch(InterruptedException ex){
            System.out.println(ex.toString());
        }
       
    }
}
