/*
 * EncertaNumeroServer. Aquest programa ens permet "hostejar" el joc d'encertar un numero,
 * comunicant-nos amb el client per poder jugar
 * @author Ferran
 * data de creació: 29/4/2019
 */

//Package
package encertanumeroserver;


//Imports
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class EncertaNumeroServer {

   
    private static final int PORT = 5000;
    private static int numero;
    
    public static void main(String[] args) {
        
        try{
            //Creem el socket i acceptem les conexions
            ServerSocket serverSocket = new ServerSocket(PORT);
            Socket socket = serverSocket.accept();
            
            //Generem el numero aleatori
            numero = generarNumero();
            int numeroUsuari = -1;
            
            //Mentre el numero de l'usuari sigui diferent del numero generat
            while(!comprovarNumero(numeroUsuari)){
                System.out.println("numero correcte: " + numero);
                
                //Rebem el numero del usuari
                numeroUsuari = rebreNumero(socket);
                System.out.println("numero Usuari: " + numeroUsuari);
                
                
                if(!comprovarNumero(numeroUsuari)){
                    if(numeroUsuari > numero){
                        enviarString(socket, "MÉS PETIT");
                    }else{
                        enviarString(socket, "MÉS GRAN");
                    }
                }else{
                    enviarString(socket, "IGUALS");
                }
            }
               
            System.out.println("JOC ACABAT");
            
        }catch(Exception e){
            System.out.println(e.toString());
        }
        
    }
    
    
    /*
     * Funcio generarNumero.
     * no rep res per parametre
     * retorna un int generat aleatoriament
    */
    public static int generarNumero(){
        int num = (int)(Math.random()* 999999)+1;
        
        return num;
    }
    
    /*
     * Funcio comprovarNumero.
     * rep un int per parametre
     * retorna un boolea segons si els dos numeros son iguals
    */    
    public static boolean comprovarNumero(int numeroUsuari){
        return numeroUsuari == numero;
    }
    
    
    /*
     * Funcio rebreNumero.
     * rep un Socket com a parametre
     * retorna el int llegit
     * llença una excepcio
     * Captura un int per el Stream del socket
    */
    public static int rebreNumero(Socket socket) throws Exception{
        int num=-1;
        try{
            InputStream is = socket.getInputStream();
            DataInputStream ois = new DataInputStream(is);
            num = (int)ois.readInt();
        }catch(Exception e)
        {
            throw e;
        }
        
        return num;
    }
    
    /*
     * Funcio enviarString.
     * rep un Socket com a parametre i un string, que es la cadena a enviar
     * no retorna res
     * llença una excepcio
     * Envia un string per el Stream del socket
    */
    private static void enviarString(Socket socket, String msg) throws Exception
    {
        try
        {
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(msg);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
}
