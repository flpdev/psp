/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enviararxiu;

import StreamUtils.StreamUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Ferran
 */
public class EnviarArxiu {

    private static final int PORT = 5000;
    private static final String PATH = "files" + File.separator;

    public static void main(String[] args) {

        System.out.println("IP del servidor: ");
        String ip = new Scanner(System.in).nextLine();
        if (ip.equals("")) {
            ip = "localhost";
        }

        try {

            int opcio = 0;
            do {
                
                opcio = menu();
                Socket socket = new Socket(ip, PORT);
                StreamUtils.enviarInt(socket, opcio);
                
                switch (opcio) {
                    case 1:
                        enviarFitxer(socket);                      
                        break;
                    case 2:
                        String filename = demanarNomFitxer();
                        rebreFitxer(socket, filename);
                        break;
                        
                }
                socket.close();
            } while (opcio != 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void enviarFitxer(Socket socket) {

        
        String fitxer = demanarNomFitxer();

        File file = new File(PATH + fitxer);
        
        try {
            if (file.exists()) {
                StreamUtils.enviarString(socket, fitxer);
                FileInputStream fis = new FileInputStream(PATH + fitxer);
                BufferedInputStream bis = new BufferedInputStream(fis);

                int caracter = bis.read();
                

                while (caracter != -1) {
                    
                    StreamUtils.enviarByte(socket, caracter);
                    caracter = bis.read();
                }

                bis.close();
                fis.close();
                System.out.print("\n");
            } else {

                System.out.println("El fitxer no existeix");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    public static void rebreFitxer(Socket socket, String filename) {

        try {
            
            FileOutputStream fos = new FileOutputStream(PATH + filename);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int caracter = StreamUtils.rebreByte(socket);

            while (caracter != -1) {

                bos.write(caracter);
                caracter = StreamUtils.rebreByte(socket);
            }

            
            bos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int menu() {
        int opcio = -1;

        System.out.println("1.- Enviar fitxer");
        System.out.println("2.- Rebre fitxer");
        System.out.println("0.- SORTUT");
        opcio = new Scanner(System.in).nextInt();

        return opcio;
    }
    
    public static String demanarNomFitxer(){
        System.out.println("Nom del fitxer: ");
        String fitxer = new Scanner(System.in).nextLine();
        
        return fitxer;
    }

}
