/*
 * IntercanviFrases. Permet rebre una serie de strings, eliminar-ne un i introduir un altre
 * @author Ferran
 * data de creació: 29/4/2019
 */

//Package
package intercanvifrasesserver;


//Imports
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;


public class IntercanviFrasesServer {

    public static int numConexions;
    public static final int PORT = 500;
    public static final String FITXER = "linies.txt";
    public static ArrayList<String> linies;
    
    
    public static void main(String[] args) {
       
        //Demanem el numero de conexions
        System.out.println("Quantes conexions vols acceptar? ");
        numConexions = new Scanner(System.in).nextInt();
        
        try{
            //Creeme el serverSocket  i el socket
            ServerSocket serverSocket = new ServerSocket(PORT);
            Socket socket;
            
            //Per cada conexio:
            for(int i=0; i<numConexions; i++){
                //resetejem el arrayList de Strings
                linies = new ArrayList<>();
                
                //acceptem la conexio
                socket = serverSocket.accept();
                
                //llegim el fitxer
                llegirStringFitxer();
                
                //enviem les linies per el stream
                enviarLinies(socket, linies);
                
                //rebem les linies modificades per el usuari
                linies = rebreLinies(socket);
                
                //guardem les cadenes al fitxer
                escriureLinies();
                
                
                //tanquem la conexio
                socket.close();
            }
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        
    }
    
    
    /*
     * Funcio llegirStringFitxer.
     * no rep cap parametre
     * no retorna res
     * llegeix el fitxer i guarda les cadenes al arrayList static linies
    */
    public static void llegirStringFitxer(){
        
        try{
            FileReader fr = new FileReader(FITXER);
            BufferedReader br = new BufferedReader(fr);
            
            String linia = br.readLine();
            
            while(linia != null){
                linies.add(linia);
                linia=br.readLine();
            }
            
            fr.close();
            br.close();
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        
    }
    
    
    /*
     * Funcio enviarLinies.
     * rep un socket per parametre
     * no retorna res
     * llença IOException
     * envia el arrayList linies per el outputStream del Socket
    */
    public static void enviarLinies(Socket socket, ArrayList<String> linies) throws IOException{
        
        try{
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(linies);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    /*
     * Funcio rebreLinies.
     * rep un socket per parametre
     * retorna un ArrayList de Strings amb les linies enviades per el servidor
     * llença IOException
    */
    public static ArrayList<String> rebreLinies(Socket socket) throws IOException{
        ArrayList<String> linies = new ArrayList<>();
        try{
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            linies = (ArrayList<String>)ois.readObject();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return linies;
    }
    
    
    /*
     * Funcio escriureLinies.
     * no rep cap parametre
     * no retorna res
     * sobreescriu el fitxer amb les linies modificades per l'aplicacio client
    */
    public static void escriureLinies(){
        try{
            FileWriter fr = new FileWriter(FITXER);
            
            for(String s: linies){
                fr.write(s+"\n");
            }
            
            fr.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
}
