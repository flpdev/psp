/*
 * Aquest programa ens permet buscar una cadena dins un directori, incloent tots els fitxers i subdirectoris
 * Data de creacio: 25/2/2019
 * @author Ferran
 */
package grep;

import java.io.File;
import java.util.Scanner;


public class Grep {

    public static void main(String[] args) {
       
       //Demanem la cadena i el directori principal al usuari
        System.out.println("Cadena a buscar: ");
        String cadena = new Scanner(System.in).nextLine();
        System.out.println("path: ");
        String path = new Scanner(System.in).nextLine();
        
        
        //Creem un Proces amb les dades introduides i l'executem
        Proces p = new Proces(new File(path), cadena);
        p.start();
    }
    
}
