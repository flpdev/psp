/*
 * Proces que busca per a cada directori o fitxer si conte una cadena, tambe busca en tots els subdirectoris possibles
 * Data de creacio: 25/2/2019
 * @author Ferran
 */
package grep;


//Imports
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Proces extends Thread {
    private File file;
    private String cadena;

    public Proces(File file, String cadena) {
        this.file = file;
        this.cadena = cadena;
    }
    
    @Override
    public void run(){
        
        try{
            //si el file es un directori, mirem en tots els seus subdirectoris/fitxers creant un altre Proces i aixi successivament
            if(file.isDirectory()){
                for(File f: file.listFiles()){
                    Proces p = new Proces(f, cadena); 
                    p.start();
                }
            }else{
                //Si es un fitxer, el llegim linia a linia, i mirem si conte la cadena
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
                
                String linia = br.readLine();
                int numLinia = 1;
                
                while(linia!=null){
                    //Si conte la cadena la mostrem per pantalla
                    if(linia.contains(cadena)){
                        System.out.println("Arxiu: " + file.getPath()+ " linia: " + numLinia + " Text: "+linia);
                    }
                    numLinia++;
                    linia = br.readLine();
                }
                
                //Tanquem el FileReader i el BufferedReader
                fr.close();
                br.close();
                
            }
        }catch(FileNotFoundException ex){
            System.out.println(ex.toString());
        }catch(IOException ex2){
            System.out.println(ex2.toString());
        }
    }
}
