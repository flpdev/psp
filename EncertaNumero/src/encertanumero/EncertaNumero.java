/*
 * EncertaNumero. Aquest programa ens permet jugar a encertar un numero entre 0 i 999999,
 * comunicant-nos amb el servidor per poder jugar
 * @author Ferran
 * data de creació: 29/4/2019
 */

//Package
package encertanumero;


//imports
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;


public class EncertaNumero {

    private static final int PORT = 5000;
    
    public static void main(String[] args) {
        
        
        //demanem la ip del servidor al usuari
        System.out.println("IP del servidor: ");
        String ip = new Scanner(System.in).nextLine();
        
        
       
        try{
            //creem el socket per conectar al servidor
            Socket socket = new Socket(ip, PORT);
            String msg = "";
            
            //Mentre el numero sigui incorrecte
            while(!"IGUALS".equals(msg)){
                
                //Demanem el numero i l'enviem al servidor
                System.out.println("Enviar numero: ");
                int num = new Scanner(System.in).nextInt();
                enviarInt(socket, num);
                
                //rebem el string resposta del servidor
                msg = rebreString(socket);
                
                //ensenyem el missatge rebur per pantall
                if(!"IGUALS".equals(msg)){
                    System.out.println(msg);
                }
            }
            
            
            System.out.println("Fi del joc, has guanyat");
            
            
        }catch(Exception e){
           System.out.println(e.toString());
                   
       }
    }
    
    
    
    /*
     * Funcio rebreString.
     * rep un Socket com a parametre
     * retorna el String llegit
     * llença una excepcio
     * Captura un string per el Stream del socket
    */
    public static String rebreString(Socket socket) throws Exception{
        String msg;
        
        try
        {
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            msg = (String) ois.readObject();
        }
        catch(Exception e)
        {
            throw e;
        }
        return msg;
    }
    
    
    /*
     * Funcio enviarInt.
     * rep un Socket com a parametre i un int, que es el numero a enviar
     * no retorna res
     * llença una excepcio
     * Envia un int per el Stream del socket
    */
    private static void enviarInt(Socket socket, int num) throws Exception
    {
        try
        {
            OutputStream os = socket.getOutputStream();
            DataOutputStream oos = new DataOutputStream(os);
            oos.writeInt(num);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
}
