/*
 * IntercanviFrases. Permet rebre una serie de strings, eliminar-ne un i introduir un altre
 * @author Ferran
 * data de creació: 29/4/2019
 */

//Package
package intercanvifrases;


//Imports
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;


public class IntercanviFrases {

    public static final int PORT = 500;
    public static ArrayList<String> linies;
    
    public static void main(String[] args) {
        
        //Demanem la direccio ip del host
        System.out.println("Direccio ip del servidor: ");
        String ip = new Scanner(System.in).nextLine();

        try{
            //Creem el socket amb la ip i el port
            Socket socket = new Socket(ip, PORT);
            
            //Rebem les linies i les mostrem per pantalla
            linies = rebreLinies(socket);
            for(String s: linies){
                System.out.println(s);
            }
            
            //Eliminem una linia
            eliminarLinia();
            
            //Tornem a enviar les linies al servidor
            enviarLinies(socket);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
       
    }
    
    
    /*
     * Funcio rebreLinies.
     * rep un socket per parametre
     * retorna un ArrayList de Strings amb les linies enviades per el servidor
     * llença IOException
    */
    public static ArrayList<String> rebreLinies(Socket socket) throws IOException{
        ArrayList<String> linies = new ArrayList<>();
        try{
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            linies = (ArrayList<String>)ois.readObject();
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        return linies;
    }
    
    /*
     * Funcio eliminarLinia.
     * no rep cap parametre
     * no retorna res
     * elimina una linia del arrayList linies, i demana una nova cadena al usuari i la afegeix al arrayList linies
    */
    public static void eliminarLinia(){
        
        //Demanem la cadena que el usuari vol eliminar
        System.out.println("Quina linia vols eliminar? 0-5: ");
        int index = -1;
        do{
            index = new Scanner(System.in).nextInt();
            
            if(index <= 0 || index > 5){
                System.out.println("numero invalid");
                System.out.println("Quina linia vols eliminar? 0-5: ");
            }
        }while(index <= 0 || index > 5);
        
        
        //borrem la linia seleccionada
        linies.remove(index-1);
        
        
        //Demanem la nova cadena a introduir
        String linia = "";
        System.out.println("Introdueix la nova linia");
        
        do{
            linia = new Scanner(System.in).nextLine();
            
            if(linia.equals("")){
                System.out.println("No pot ser una cadena buida");
                System.out.println("Introdueix la nova linia");
            }
            
        }while(linia.equals(""));
        
        //Afegim la cadena al arrayList de linies
        linies.add(linia);
    }
    
    
    /*
     * Funcio enviarLinies.
     * rep un socket per parametre
     * no retorna res
     * llença IOException
     * envia el arrayList linies per el outputStream del Socket
    */
    public static void enviarLinies(Socket socket) throws IOException{
        
        try{
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(linies);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        
    }
    
}
