/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enviararxiuserver;

import StreamUtils.StreamUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Ferran
 */
public class EnviarArxiuServer {

    private static final int PORT = 5000;
    private static final String PATH = "files" + File.separator;

    public static void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            Socket socket;

            while (true) {
                socket = serverSocket.accept();
                int opcio;
                opcio = StreamUtils.rebreNumero(socket);
                String fileName = StreamUtils.rebreString(socket);
                
                switch(opcio){
                    case 1:
                        
                        rebreFitxer(socket, fileName);
                        break;
                        
                    case 2:
                        
                        enviarFitxer(socket, fileName);
                        break;
                }

               socket.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void rebreFitxer(Socket socket, String filename) {

        try {
            
            FileOutputStream fos = new FileOutputStream(PATH + filename);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            int caracter = StreamUtils.rebreByte(socket);

            while (caracter != -1) {

                bos.write(caracter);
                caracter = StreamUtils.rebreByte(socket);
            }

            
            bos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public static void enviarFitxer(Socket socket, String filename){
        
        if(checkFile(filename)){
            
            try{
                FileInputStream fis = new FileInputStream(PATH + filename);
                BufferedInputStream bis = new BufferedInputStream(fis);

                int caracter = bis.read();

                while (caracter != -1) {
                    
                    StreamUtils.enviarByte(socket, caracter);
                    caracter = bis.read();
                }
                
                bis.close();
                fis.close();
                
            }catch(Exception e){
                e.printStackTrace();
            }
            
        }else{
            System.out.println("El fitxer no existeix");
        }
    }
    
    public static boolean checkFile(String filename){
        return new File(PATH+filename).exists();
    }

}
