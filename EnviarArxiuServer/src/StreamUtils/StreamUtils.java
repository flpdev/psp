/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StreamUtils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author Ferran
 */
public class StreamUtils {
    
    public static void enviarInt(Socket socket, int num) throws Exception{
        try{
            
            OutputStream is = socket.getOutputStream();
            DataOutputStream dis = new DataOutputStream(is);
            dis.write(num);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static int rebreNumero(Socket socket) throws Exception{
        int num=-1;
        try{
            InputStream is = socket.getInputStream();
            num = is.read();
        }catch(Exception e)
        {
            throw e;
        }
        
        return num;
    }
    
    public static void enviarByte(Socket socket, int caracter) throws Exception{
        
        try{
            OutputStream os = socket.getOutputStream();
            os.write(caracter);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static int rebreByte(Socket socket) throws Exception{
        int b = 0;
        try{
            InputStream is = socket.getInputStream();
            b = is.read();
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return b;
    }
    
    public static String rebreString(Socket socket) throws Exception{
        String msg;
        
        try
        {
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            msg = (String) ois.readObject();
        }
        catch(Exception e)
        {
            throw e;
        }
        return msg;
    }
    
    /*
     * Funcio enviarString.
     * rep un Socket com a parametre i un string, que es la cadena a enviar
     * no retorna res
     * llença una excepcio
     * Envia un string per el Stream del socket
    */
    public static void enviarString(Socket socket, String msg) throws Exception
    {
        try
        {
            OutputStream os = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(msg);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}